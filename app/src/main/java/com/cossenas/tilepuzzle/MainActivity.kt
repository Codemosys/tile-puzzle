package com.cossenas.tilepuzzle

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.cossenas.tilepuzzle.tiles.TilesFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, TilesFragment.newInstance())
                .commitNow()
        }
    }

}
