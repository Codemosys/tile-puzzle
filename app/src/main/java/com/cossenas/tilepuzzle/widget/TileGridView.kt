package com.cossenas.tilepuzzle.widget

import android.content.Context
import android.util.AttributeSet
import android.view.GestureDetector
import android.view.MotionEvent
import android.widget.GridView
import kotlin.math.abs
import kotlin.math.roundToInt


class TileGridView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : GridView(context, attrs, defStyleAttr) {

    companion object {
        const val SWIPE_MINIMUM = 100
        const val SWIPE_MAXIMUM = 100
        const val SWIPE_VELOCITY = 100
    }

    enum class MoveDirection {
        UP, DOWN, LEFT, RIGHT
    }

    interface TileGridViewMoveListener {
        fun onMoveTile(direction: MoveDirection, position: Int)
    }

    private var gDetector: GestureDetector
    private var moveListener: TileGridViewMoveListener? = null
    private var didFling = false
    private var touchAtX: Float = 0f
    private var touchAtY: Float = 0f

    init {

        gDetector = GestureDetector(context, object : GestureDetector.SimpleOnGestureListener() {
            override fun onFling(
                e1: MotionEvent?,
                e2: MotionEvent?,
                velocityX: Float,
                velocityY: Float
            ): Boolean {

                if (e1 != null && e2 != null && moveListener != null) {
                    val position = this@TileGridView.pointToPosition(
                        e1.x.roundToInt(),
                        e1.y.roundToInt()
                    )

                    if (abs(e1.y - e2.y) > SWIPE_MAXIMUM) {
                        if (abs(e1.x - e2.x) > SWIPE_MAXIMUM || abs(velocityY) < SWIPE_VELOCITY) return false

                        if (e1.y - e2.y > SWIPE_MINIMUM) {
                            moveListener!!.onMoveTile(MoveDirection.UP, position)
                        } else if (e2.y - e1.y > SWIPE_MINIMUM) {
                            moveListener!!.onMoveTile(MoveDirection.DOWN, position)
                        }

                    } else {
                        if (abs(velocityX) < SWIPE_VELOCITY) return false

                        if (e1.x - e2.x > SWIPE_MINIMUM) {
                            moveListener!!.onMoveTile(MoveDirection.LEFT, position)
                        } else if (e2.x - e1.x > SWIPE_MINIMUM) {
                            moveListener!!.onMoveTile(MoveDirection.RIGHT, position)
                        }
                    }
                }
                return super.onFling(e1, e2, velocityX, velocityY)
            }

            override fun onDown(event: MotionEvent?): Boolean {
                return true
            }
        })

    }

    override fun onInterceptTouchEvent(ev: MotionEvent): Boolean {
        val action = ev.actionMasked
        gDetector.onTouchEvent(ev)

        if (action == MotionEvent.ACTION_CANCEL || action == MotionEvent.ACTION_UP) {
            didFling = false
        } else if (action == MotionEvent.ACTION_DOWN) {
            touchAtX = ev.x
            touchAtY = ev.y
        } else {

            if (didFling) {
                return true
            }

            val dX = Math.abs(ev.x - touchAtX)
            val dY = Math.abs(ev.y - touchAtY)
            if (dX > SWIPE_MINIMUM || dY > SWIPE_MINIMUM) {
                didFling = true
                return true
            }
        }

        return super.onInterceptTouchEvent(ev)
    }

    override fun onTouchEvent(ev: MotionEvent): Boolean {
        return gDetector.onTouchEvent(ev)
    }

    fun setMoveListener(listener: TileGridViewMoveListener) {
        moveListener = listener
    }

}