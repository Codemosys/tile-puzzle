package com.cossenas.tilepuzzle.widget

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.widget.TextView
import com.cossenas.tilepuzzle.R

class TileView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : TextView(context, attrs, defStyleAttr) {

    init {
        // Set up initial tile
        setBackgroundResource(R.drawable.tile_background)
        setTypeface(typeface, Typeface.BOLD)
        setTextSize(TypedValue.COMPLEX_UNIT_SP, 30f)
        gravity = Gravity.CENTER
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec)
    }

    fun setTileId(id: Int) {
        text = id.toString()
    }

    fun setAsBlank() {
        setBackgroundColor(context.getColor(android.R.color.transparent))
    }
}