package com.cossenas.tilepuzzle.tiles

import android.app.AlertDialog
import android.content.Context
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.os.VibrationEffect
import android.os.Vibrator
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.lifecycle.Observer
import com.cossenas.tilepuzzle.R
import com.cossenas.tilepuzzle.widget.TileGridView

class TilesFragment : Fragment(), TileGridView.TileGridViewMoveListener {

    companion object {
        fun newInstance() = TilesFragment()
    }

    private lateinit var viewModel: TilesViewModel
    private lateinit var gridTiles: TileGridView
    private lateinit var tileAdapter: TileAdapter
    private lateinit var btnRestart: Button
    private lateinit var rb3x3: RadioButton
    private lateinit var rb4x4: RadioButton
    private lateinit var rb5x5: RadioButton
    private lateinit var vibrate: Vibrator

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = inflater.inflate(R.layout.fragment_tiles, container, false)

        btnRestart = view.findViewById(R.id.btn_restart)
        rb3x3 = view.findViewById(R.id.rb_3x3)
        rb4x4 = view.findViewById(R.id.rb_4x4)
        rb5x5 = view.findViewById(R.id.rb_5x5)

        // Redrawing grid at different sizes does not work :-( so lets hide it
        view.findViewById<RadioGroup>(R.id.rg_tiles_count).visibility = View.GONE

        gridTiles = view.findViewById(R.id.grid_puzzle_tiles)

        tileAdapter = TileAdapter(context!!)
        gridTiles.adapter = tileAdapter

        gridTiles.setMoveListener(this)

        setupListeners()

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(TilesViewModel::class.java)

        vibrate = activity?.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator

        setupObservers()
    }

    private fun setupObservers() {

        viewModel.getCurrentNumOfColumns().observe(this, Observer<Int> { cols ->
            cols?.let { columns ->
                gridTiles.numColumns = columns
                gridTiles.refreshDrawableState()
                rb3x3.isChecked = columns == 3
                rb4x4.isChecked = columns == 4
                rb5x5.isChecked = columns == 5
            }
        })

        viewModel.getCurrentTileData().observe(this, Observer<Array<Int>> { data ->
            data?.let {
                tileAdapter.setData(it)
            }
        })

        viewModel.getOnInvalidMove().observe(this, Observer { invalid ->
            invalid?.let {
                if (it) vibrateFeedback()
            }
        })

        viewModel.getOnYouWin().observe(this, Observer { win ->
            win?.let { youWin ->
                if (youWin) showYouWinDialog()
            }
        })
    }

    private fun setupListeners() {
        btnRestart.setOnClickListener {
            viewModel.restartGame()
        }

        rb3x3.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) viewModel.changeGridSize(3)
        }

        rb4x4.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) viewModel.changeGridSize(4)
        }

        rb5x5.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) viewModel.changeGridSize(5)
        }

    }

    override fun onMoveTile(direction: TileGridView.MoveDirection, position: Int) {
        viewModel.moveTile(direction, position)
    }

    private fun vibrateFeedback() {
        if (vibrate.hasVibrator()) {
            vibrate.vibrate(VibrationEffect.createOneShot(200, VibrationEffect.DEFAULT_AMPLITUDE))
        }
    }

    private fun showYouWinDialog() {
        AlertDialog.Builder(context)
            .setMessage(R.string.you_win)
            .setPositiveButton(android.R.string.ok) { _, _ -> viewModel.restartGame()}
            .show()
    }
}
