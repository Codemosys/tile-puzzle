package com.cossenas.tilepuzzle.tiles

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cossenas.tilepuzzle.widget.TileGridView
import com.cossenas.tilepuzzle.helpers.SingleLiveEvent
import java.util.*

class TilesViewModel : ViewModel() {

    companion object {
        const val INITIAL_COLUMN_COUNT = 4
    }

    private val currentTileData = MutableLiveData<Array<Int>>()
    fun getCurrentTileData(): MutableLiveData<Array<Int>> = currentTileData

    private val onInvalidMove = SingleLiveEvent<Boolean>()
    fun getOnInvalidMove(): SingleLiveEvent<Boolean> = onInvalidMove

    private val onYouWin = MutableLiveData<Boolean>()
    fun getOnYouWin(): MutableLiveData<Boolean> = onYouWin

    private val currentNumOfColumns = MutableLiveData<Int>()
    fun getCurrentNumOfColumns(): MutableLiveData<Int> = currentNumOfColumns

    private lateinit var tileData: Array<Int>
    private var numOfColumns = INITIAL_COLUMN_COUNT
    private var numOfTiles = numOfColumns * numOfColumns

    init {
        onYouWin.value = false
        currentNumOfColumns.value = INITIAL_COLUMN_COUNT
        restartGame()
        scrambleTiles()
        currentTileData.value = tileData
    }

    private fun scrambleTiles() {
        var index: Int
        var temp: Int
        val random = Random()

        for (i in tileData.size - 1 downTo 1) {
            index = random.nextInt(i + 1)
            temp = tileData[index]
            tileData[index] = tileData[i]
            tileData[i] = temp
        }
    }

    fun restartGame() {
        tileData = setupTileData(numOfTiles)
        scrambleTiles()
        currentTileData.value = tileData
    }

    fun changeGridSize(numOfCols: Int) {
        numOfColumns = numOfCols
        numOfTiles = numOfColumns * numOfColumns
        restartGame()
    }

    fun moveTile(direction: TileGridView.MoveDirection, position: Int) {

        when {
            // Top right
            position == numOfColumns - 1 -> {
                when (direction) {
                    TileGridView.MoveDirection.LEFT -> moveIfPossible(position, -1)
                    TileGridView.MoveDirection.DOWN -> moveIfPossible(position, numOfColumns)
                    else -> {}
                }
            }
            // Left
            position > numOfColumns - 1 && position < numOfTiles - numOfColumns && position % numOfColumns == 0 -> {
                when (direction) {
                    TileGridView.MoveDirection.UP -> moveIfPossible(position, -numOfColumns)
                    TileGridView.MoveDirection.RIGHT -> moveIfPossible(position, 1)
                    TileGridView.MoveDirection.DOWN -> moveIfPossible(position, numOfColumns)
                    else -> {}
                }
            }
            // Right
            position == (numOfColumns * 2) - 1
                    || position == (numOfColumns * 3) - 1
                    || position == (numOfColumns * 4) - 1 -> { // Be great to improve on this for scaling
                when (direction) {
                    TileGridView.MoveDirection.UP -> moveIfPossible(position, -numOfColumns)
                    TileGridView.MoveDirection.LEFT -> moveIfPossible(position, -1)
                    TileGridView.MoveDirection.DOWN -> moveIfPossible(position, numOfColumns)
                    else -> {
                    }
                }
            }
            // Bottom left
            position == numOfTiles - numOfColumns -> {
                when (direction) {
                    TileGridView.MoveDirection.UP -> moveIfPossible(position, -numOfColumns)
                    TileGridView.MoveDirection.RIGHT -> moveIfPossible(position, 1)
                    else -> {}
                }
            }
            else -> {
                when (direction) {
                    TileGridView.MoveDirection.UP -> moveIfPossible(position, -numOfColumns)
                    TileGridView.MoveDirection.LEFT -> moveIfPossible(position, -1)
                    TileGridView.MoveDirection.RIGHT -> moveIfPossible(position, 1)
                    TileGridView.MoveDirection.DOWN -> moveIfPossible(position, numOfColumns)
                }
            }
        }
    }

    private fun moveIfPossible(currentPos: Int, shift: Int) {
        val newIndex = currentPos + shift
        if (newIndex in 0 until numOfTiles) {
            val newPosition = tileData[newIndex]
            if (newPosition == numOfTiles) { // Moving to the empty position
                tileData[currentPos + shift] = tileData[currentPos]
                tileData[currentPos] = newPosition
                currentTileData.value = tileData
            } else {
                onInvalidMove.value = true
            }
            onYouWin.value = checkForWin()
        }
    }

    private fun setupTileData(numOfTiles: Int): Array<Int> {
        val data = IntArray(numOfTiles) { i -> i + 1 }
        return data.toTypedArray()
    }

    private fun checkForWin(): Boolean {
        var match = false
        for (i in tileData.indices) {
            if (tileData[i] == i+1) {
                match = true
            } else {
                match = false
                break
            }
        }
        return match
    }
}
