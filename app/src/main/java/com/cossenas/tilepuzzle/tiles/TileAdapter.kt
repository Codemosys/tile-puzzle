package com.cossenas.tilepuzzle.tiles

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.cossenas.tilepuzzle.widget.TileView

class TileAdapter(private val context: Context) : BaseAdapter() {

    private var tileData = arrayOf(1)

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val tileView = TileView(context)
        val id = tileData[position]
        if (id != tileData.size) {
            tileView.setTileId(id)
        } else {
            tileView.setAsBlank()
        }

        return tileView
    }

    override fun getItem(position: Int): Any {
        return tileData[position]
    }

    override fun getItemId(position: Int): Long {
        return tileData[position].toLong()
    }

    override fun getCount(): Int {
        return tileData.size
    }

    fun setData(newData: Array<Int>) {
        tileData = newData
        notifyDataSetChanged()
    }
}